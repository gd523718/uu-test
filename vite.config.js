import { createVuePlugin } from "vite-plugin-vue2";
import { defineConfig } from "vite";
import { resolve } from "path";

export default defineConfig({
  resolve: {
    alias: [{ find: "@", replacement: resolve(__dirname, "/src") }]
  },
  base: "/",
  plugins: [createVuePlugin()],
  server: {
    host: "0.0.0.0"
  }
});
