import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    redirect: "merchantAnalysis" // 重定向
  },
  {
    path: "/home",
    meta: { title: "首页", keepAlive: true },
    component: () => import("../views/Home.vue")
  },
  {
    path: "/myShop",
    component: () => import("../views/MyShop.vue")
  },
  {
    path: "/merchantAnalysis",
    component: () => import("../views/MerchantAnalysis.vue")
  }
];

const router = new VueRouter({
  mode: "hash",
  // base: process.env.BASE_URL,
  routes
});

export default router;
