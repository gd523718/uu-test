export interface GoodsItme {
  GoodsPicUrl: string;
  ActivityId: string;
  StartTime: string;
  OriginalGoodsPrice: number;
  GoodsName: string;
  GoodsCount: number;
  GoodsUnit: string;
  EndTime: string;
  GoodsPrice: number;
  Sort: number;
  GoodsId: string;
}

export interface GoodsCategoryItem {
  GoodsList: GoodsItme[];
  CategoryId: number;
  Name: string;

  isActive?: boolean;
}
export interface ShopInfo {
  State: number;
  Body: {
    Sale: number | string;
    BusinessEndTime: string;
    BusinessStartTime: string;
    MerchantName: string;
    MerchantThumbnail: string;
    Distance: number;
    GoodsCategoryList: GoodsCategoryItem[];
  };
}
