import { request } from "@/server";
import type { ShopInfo } from "@/types/shop";
import shopInfo from "./shopInfo.json";

/**
 *  获取店铺信息接口
 * @returns {Promise<ShopInfo>}
 */
export const getShopInfo = (): Promise<ShopInfo> =>
  new Promise((resolve, reject) => {
    resolve(shopInfo as ShopInfo);
  });

export interface ShopItemInfo {
  id: number;
  name: string;
  address: string;
  addressLoc: { lng: string; lat: string };
  isDefault: boolean;
  cityName: string;
  coutyName: string;
  linkMan: string;
  /** 手机号 */
  linkManMobile: string;
}

export const getMyShopInfo = () => request<ShopItemInfo[]>("/mock/getShopInfo");

export interface ShopOrderItem {
  address: string;
  value: number;
  detail: {
    id: number;
    point: { lng: string; lat: string };
    totalCount: number;
    todayCount: number;
    day3Count: number;
    day7Count: number;
    day30Count: number;
    day90Count: number;
  };
}

export const getOrderlist = () =>
  request<{
    distance: number;
    stats: ShopOrderItem[];
  }>("/mock/getOrderlist");

export interface CustomerItem {
  time: string;
  value: number;
}
export const getCustomerlist = () => request<CustomerItem[]>("/mock/getCustomerlist");
