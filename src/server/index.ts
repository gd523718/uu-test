export interface ResponseType<T = any> {
  State: number;
  Msg: string;
  Body: T;
}
export const request = <T = any>(api: string) => {
  return new Promise<T>((resolve, reject) => {
    const baceUrl = "http://127.0.0.1:9000"; // 本地模拟数据地址
    api = api.startsWith("/") ? api : `/${api}`;
    api = `${baceUrl}${api}`;
    try {
      fetch(api)
        .then(response => response.json())
        .then(data => resolve(data.Body));
    } catch (error) {
      reject(error);
    }
  });
};
