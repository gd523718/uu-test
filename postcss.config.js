module.exports = {
  plugins: {
    "postcss-pxtorem": {
      rootValue: 75,
      propList: ["*"],
      selectorBlackList: ["ant", "f2", "@antv"]
    }
  }
};
