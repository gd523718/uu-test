const { wrapApiData } = require(`../util.js`);

/** @type {import('mockm/@types/config').Config} */
module.exports = util => {
  const {
    libObj: { mockjs }
  } = util;
  return {
    api: {
      // 创建接口并使用 mockjs 生成数据
      "get /api/test": wrapApiData(
        mockjs.mock({
          "data|3-7": [
            {
              userId: `@id`,
              userName: `@cname`
            }
          ]
        })
      ),
      // 店铺列表
      "get /mock/getShopInfo": {
        State: 1,
        Body: [
          {
            id: 1,
            name: "蛋糕烘焙定制",
            address: "经三路红旗路",
            addressLoc: { lng: "113.724495", lat: "34.768917" },
            isDefault: true,
            cityName: "郑州",
            coutyName: "金水区",
            linkMan: "张三",
            linkManMobile: "13923243544"
          },
          {
            id: 2,
            name: "蛋糕烘焙定制（Polly）",
            address: "二七万达广场",
            addressLoc: { lng: "113.777372", lat: "34.767533" },
            isDefault: false,
            cityName: "郑州市",
            coutyName: "二七区",
            linkMan: "李四",
            linkManMobile: "13456763343"
          },
          {
            id: 3,
            name: "蛋糕烘焙定制",
            address: "中原锦艺城",
            addressLoc: { lng: "113.726486", lat: "34.753384" },
            isDefault: false,
            cityName: "郑州市",
            coutyName: "中原区",
            linkMan: "赵六",
            linkManMobile: "19923243544"
          }
        ],
        Msg: ""
      },

      // 订单数据统计
      "get /mock/getOrderlist": {
        State: 1,
        Body: {
          distance: 5,
          stats: [
            {
              address: "楷林国际大厦",
              value: 19,
              detail: {
                id: 1,
                point: { lng: "113.722385", lat: "34.769986" },
                totalCount: 100,
                todayCount: 5,
                day3Count: 12,
                day7Count: 35,
                day30Count: 57,
                day90Count: 79
              }
            },
            {
              address: "名门城市广场",
              value: 21,
              detail: {
                id: 2,
                point: { lng: "113.722385", lat: "34.769619" },
                totalCount: 52,
                todayCount: 3,
                day3Count: 11,
                day7Count: 23,
                day30Count: 44,
                day90Count: 50
              }
            },
            {
              address: "广汇PAMA",
              value: 13,
              detail: {
                id: 3,
                point: { lng: "113.721715", lat: "34.768852" },
                totalCount: 66,
                todayCount: 5,
                day3Count: 12,
                day7Count: 35,
                day30Count: 57,
                day90Count: 66
              }
            },
            {
              address: "曼哈顿生活广场",
              value: 15,
              detail: {
                id: 4,
                point: { lng: "113.723777", lat: "34.767073" },
                totalCount: 32,
                todayCount: 1,
                day3Count: 5,
                day7Count: 22,
                day30Count: 30,
                day90Count: 32
              }
            },
            {
              address: "升龙广场",
              value: 9,
              detail: {
                id: 5,
                point: { lng: "113.719402", lat: "34.769008" },
                totalCount: 77,
                todayCount: 1,
                day3Count: 3,
                day7Count: 6,
                day30Count: 66,
                day90Count: 75
              }
            }
          ]
        },
        Msg: ""
      },

      // 客户量统计
      "get /mock/getCustomerlist": {
        State: 1,
        Body: [
          { time: "07/30", value: 254 },
          { time: "08/01", value: 354 },
          { time: "08/02", value: 634 },
          { time: "08/04", value: 904 },
          { time: "07/28", value: 324 },
          { time: "08/05", value: 1004 },
          { time: "08/06", value: 1154 },
          { time: "08/07", value: 434 },
          { time: "08/08", value: 934 },
          { time: "08/10", value: 1564 },
          { time: "08/15", value: 854 },
          { time: "08/16", value: 254 },
          { time: "08/17", value: 1514 },
          { time: "08/18", value: 154 },
          { time: "08/20", value: 1254 },
          { time: "08/21", value: 2354 },
          { time: "08/22", value: 1854 },
          { time: "08/24", value: 811 },
          { time: "08/25", value: 194 },
          { time: "08/26", value: 354 },
          { time: "08/27", value: 154 },
          { time: "08/28", value: 154 },
          { time: "08/29", value: 894 },
          { time: "08/30", value: 154 },
          { time: "09/01", value: 564 },
          { time: "09/02", value: 744 },
          { time: "09/04", value: 554 },
          { time: "09/05", value: 1854 },
          { time: "09/06", value: 884 },
          { time: "09/07", value: 774 },
          { time: "09/08", value: 914 },
          { time: "09/09", value: 234 },
          { time: "09/10", value: 1235 },
          { time: "09/11", value: 765 },
          { time: "09/12", value: 863 },
          { time: "09/13", value: 225 },
          { time: "09/14", value: 793 },
          { time: "09/15", value: 872 },
          { time: "09/16", value: 12 },
          { time: "09/17", value: 154 },
          { time: "09/18", value: 45 },
          { time: "09/19", value: 154 },
          { time: "09/20", value: 95 },
          { time: "09/21", value: 154 },
          { time: "09/22", value: 334 },
          { time: "09/23", value: 113 },
          { time: "09/24", value: 234 },
          { time: "09/25", value: 542 },
          { time: "09/26", value: 154 },
          { time: "09/27", value: 333 },
          { time: "09/29", value: 563 },
          { time: "09/30", value: 123 },
          { time: "10/01", value: 356 },
          { time: "10/04", value: 111 },
          { time: "10/05", value: 987 },
          { time: "10/07", value: 159 },
          { time: "10/09", value: 347 },
          { time: "10/10", value: 301 },
          { time: "10/11", value: 255 },
          { time: "10/12", value: 768 },
          { time: "10/13", value: 359 },
          { time: "10/14", value: 254 },
          { time: "10/15", value: 258 },
          { time: "10/17", value: 255 },
          { time: "10/18", value: 623 },
          { time: "10/20", value: 247 },
          { time: "10/21", value: 134 },
          { time: "10/22", value: 301 },
          { time: "10/24", value: 154 },
          { time: "10/25", value: 1238 },
          { time: "10/26", value: 833 },
          { time: "10/27", value: 436 },
          { time: "10/27", value: 345 }
        ],
        Msg: ""
      }
    }
  };
};
